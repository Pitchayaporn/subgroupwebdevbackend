import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Attendance } from "./entities/attendance.entity";
import { CreateAttendanceDto } from "./dto/create-attendance.dto";
import { UpdateAttendanceDto } from "./dto/update-attendance.dto";
import { Employee } from "src/employees/entities/employee.entity";

@Injectable()
export class AttendanceService {
  constructor(
    @InjectRepository(Attendance)
    private readonly attendanceRepository: Repository<Attendance>,
    @InjectRepository(Employee)
    private readonly employeeRepository: Repository<Employee>,
  ) {}

  findAll() {
    return this.attendanceRepository.find();
  }
  findOne(id: number) {
    return this.attendanceRepository.findOne({
      where: { id },
      relations: { employee: true },
    });
  }

  remove(id: number) {
    return `This action removes a #${id} attendance`;
  }

  async createAttendance(createAttendanceDto: CreateAttendanceDto) {
    const employee = await this.employeeRepository.findOne({
      where: { id: +createAttendanceDto.employeeId },
    });
    if (!employee) {
      throw new Error("Employee not found");
    }

    const attendance = new Attendance();
    attendance.employee = employee;
    attendance.date = new Date();
    if (createAttendanceDto.clockIn) {
      attendance.clockIn = new Date();
    }
    attendance.clockOut = null;

    // Save attendance
    return await this.attendanceRepository.save(attendance);
  }
  async updateAttendance(id: number, updateAttendanceDto: UpdateAttendanceDto) {
    const attendance = await this.attendanceRepository.findOne({
      where: { id },
    });

    if (!attendance) {
      throw new Error("Attendance not found");
    }

    if (!attendance && updateAttendanceDto.clockOut) {
      throw new Error("Cannot set clock out without a corresponding clock in");
    }
    attendance.clockOut = new Date();
    console.log(updateAttendanceDto.clockOut);
    console.log(attendance.clockOut);
    console.log(attendance.clockOut);
    // return attendance.clockOut;

    if (attendance.clockIn && attendance.clockOut) {
      console.log("Clock In:", attendance.clockIn);
      console.log("Clock Out:", attendance.clockOut);

      const timeDifference =
        new Date(attendance.clockOut).getTime() -
        new Date(attendance.clockIn).getTime();
      console.log("Time Difference:", timeDifference);

      const workedTimeHours = timeDifference / (1000 * 60 * 60); // Calculate worked time in hours
      console.log("Worked Time Hours:", workedTimeHours);

      if (!isNaN(workedTimeHours)) {
        attendance.workedTime = workedTimeHours;
      } else {
        console.error("Invalid worked time");
        throw new Error("Invalid worked time");
      }
    }

    // Save the updated attendance record
    return await this.attendanceRepository.save(attendance);
  }

  async findAttendanceByEmployeeId(employeeId: number) {
    const employee = await this.employeeRepository.findOne({
      where: { id: +employeeId },
    });
    if (!employee) {
      throw new Error("Employee not found");
    }
    return await this.attendanceRepository.find({
      where: { employee: employee },
    });
  }
}
