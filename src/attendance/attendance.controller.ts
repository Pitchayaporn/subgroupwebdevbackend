import { Controller, Get, Post, Body, Param, Patch } from "@nestjs/common";
import { AttendanceService } from "./attendance.service";
import { Attendance } from "./entities/attendance.entity";
import { CreateAttendanceDto } from "./dto/create-attendance.dto";
import { UpdateAttendanceDto } from "./dto/update-attendance.dto";

@Controller("attendance")
export class AttendanceController {
  constructor(private readonly attendanceService: AttendanceService) {}
  @Get()
  findAll(): Promise<Attendance[]> {
    return this.attendanceService.findAll();
  }

  @Get(":id")
  findOne(@Param("id") id: string): Promise<Attendance> {
    return this.attendanceService.findOne(+id);
  }

  @Post()
  create(
    @Body() createAttendanceDto: CreateAttendanceDto,
  ): Promise<Attendance> {
    return this.attendanceService.createAttendance(createAttendanceDto);
  }

  @Patch(":id")
  update(
    @Param("id") id: string,
    @Body() updateAttendanceDto: UpdateAttendanceDto,
  ): Promise<Attendance> {
    return this.attendanceService.updateAttendance(+id, updateAttendanceDto);
  }
}
