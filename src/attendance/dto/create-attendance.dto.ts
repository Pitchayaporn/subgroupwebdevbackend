// create-attendance.dto.ts
import { IsString } from "class-validator";

export class CreateAttendanceDto {
  @IsString()
  employeeId: string;

  @IsString()
  date: string;

  @IsString()
  clockIn?: string;

  clockOut?: string;
}
