import { Employee } from "src/employees/entities/employee.entity";
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";

@Entity()
export class Attendance {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  employeeId: number;

  @Column({ type: "date" })
  date: Date;

  @Column({
    type: "time",
    default: () => "datetime('now', 'localtime')",
    nullable: true,
  })
  clockIn: Date;

  @Column({
    type: "time",
    default: () => "datetime('now', 'localtime')",
    nullable: true,
  })
  clockOut: Date;

  @Column({ default: 0 })
  workedTime: number;

  @ManyToOne(() => Employee, (employee) => employee.attendances)
  @JoinColumn({ name: "employeeId" })
  employee: Employee;
}
