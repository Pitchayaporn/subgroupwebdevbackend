import { Attendance } from 'src/attendance/entities/attendance.entity';
import { Status } from 'src/status/entities/status.entity';
import {
  Column,
  Entity,
  JoinTable,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Employee {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  tel: string;

  @Column()
  salary: number;

  @Column()
  dateIn: string;

  @Column()
  checkIn: string;

  @Column()
  checkOut: string;

  @ManyToOne(() => Status, (status) => status.employees)
  status: Status;

  @OneToMany(() => Attendance, (attendance) => attendance.employee)
  attendances: Attendance[];
}
