import { Injectable } from '@nestjs/common';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import { Employee } from './entities/employee.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class EmployeesService {
  constructor(
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
  ) {}
  // create(createEmployeeDto: CreateEmployeeDto): Promise<Employee> {
  //   return this.employeeRepository.save(createEmployeeDto);
  // }

  findAll() {
    return this.employeeRepository.find({ relations: { status: true } });
  }

  findOne(id: number) {
    return this.employeeRepository.findOne({
      where: { id },
      relations: { status: true },
    });
  }
  // findOne(id: number) {
  //   return this.employeeRepository.findOneBy({ id });
  // }

  // async update(id: number, updateEmployeeDto: UpdateEmployeeDto) {
  //   await this.employeeRepository.update(id, updateEmployeeDto);
  //   const employee = await this.employeeRepository.findOneBy({ id });
  //   return employee;
  // }

  // async remove(id: number) {
  //   const deleteEmployee = await this.employeeRepository.findOneBy({ id });
  //   return this.employeeRepository.remove(deleteEmployee);
  // }
}
