/* eslint-disable prettier/prettier */
import { IsNotEmpty, Length } from 'class-validator';
export class CreateSalaryDto {
  @IsNotEmpty()
  empId: number;

  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  pay: number;

  @IsNotEmpty()
  @Length(8)
  date: string;

  @IsNotEmpty()
  workHour: number;
}
